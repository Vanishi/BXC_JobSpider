### BXC_JobSpider

* 作者：北小菜 
* 个人网站：http://www.any12345.com
* 邮箱：bilibili_bxc@126.com
* QQ：1402990689
* 微信：bilibili_bxc
* 招聘数据采集软件 gitee开源地址：https://gitee.com/Vanishi/BXC_JobSpider
* 招聘数据分析软件 gitee开源地址：https://gitee.com/Vanishi/BXC_JobAnalysis

### 软件介绍
* 基于 python+selenium 开发的招聘数据采集软件
* 截止2023.12.23，本软件仅实现了，基于selenium控制浏览器，采集boss直聘的招聘数据。在采集过程中，建议放慢采集频率，避免对目标网站造成损害，以及避免遇到反爬问题，导致采集数据不完整。


### 启动软件
~~~

第一步：通过命令行启动一个指定端口的谷歌浏览器
chrome --remote-debugging-port=9555

第二步：启动程序
python main.py

~~~

### 安装

| 程序         | 版本      |
| ---------- | ------- |
| python     | 3.5+    |
| 依赖库      | requirements.txt |



#### linux 创建python虚拟环境
~~~

# 创建虚拟环境
python -m venv venv

# 激活虚拟环境
source venv/bin/activate

# 更新虚拟环境的pip版本
python -m pip install --upgrade pip -i https://pypi.tuna.tsinghua.edu.cn/simple

# 在虚拟环境中安装依赖库
python -m pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple

~~~

#### windows 创建python虚拟环境
~~~
# 创建虚拟环境
python -m venv venv

# 切换到虚拟环境
venv\Scripts\activate

# 更新虚拟环境的pip版本
python -m pip install --upgrade pip -i https://pypi.tuna.tsinghua.edu.cn/simple

# 在虚拟环境中安装依赖库
python -m pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple

~~~